import React from 'react'
const Header = (props) => (
    <div className="header">
        <img src="https://cdn4.iconfinder.com/data/icons/planner-color/64/popcorn-movie-time-512.png" className="img_header"></img>
        <div className="header_right">
        <span className="join us"> Join Us</span>
        </div>
    </div>
)
export default Header