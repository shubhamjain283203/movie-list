import React from 'react'

const WithPower = (Wcomponent) => {
   const newcomponent = (props) => {
       return(
           <div style={{backgroundColor: "red"}}>
             <Wcomponent {...props}/>
           </div>
       )
   }
   return newcomponent
}
export default WithPower